/*
======================================================
-->> Premenne
======================================================
*/
var myLatLng = {lat: 48.15174853, lng: 17.07317065};
var infowindow;
var map;
/*
======================================================
-->> Funkcie
======================================================
*/
function initialize() {

     map = new google.maps.Map(document.getElementById('map'), {
        mapTypeControl: false,
        center: myLatLng,
        zoom: 13
    });

    var marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        icon: {
        url: "http://maps.google.com/mapfiles/ms/icons/red-dot.png",
        labelOrigin: new google.maps.Point(75, 32),
        size: new google.maps.Size(32,32),
        anchor: new google.maps.Point(16,32)
        },
        label: {
        text: "FEI STU",
        color: "#C70E20",
        fontWeight: "bold"
        }

    });

    google.maps.event.addListener(marker, 'click', function() {
        alert("Latitude: " + myLatLng.lat + " Longitude: " + myLatLng.lng);
    });


    new AutocompleteDirectionsHandler(map);

    var panorama = new google.maps.StreetViewPanorama(
            document.getElementById('pano'), {
                position: myLatLng,
                pov: {
                    heading: 34,
                    pitch: 10
                }
            });
        map.setStreetView(panorama);

    infowindow = new google.maps.InfoWindow();
    var service = new google.maps.places.PlacesService(map);
    service.nearbySearch({
        location: myLatLng,
        radius: 500,
        type: ['bus_station']
    }, callback);

}

function callback(results, status) {
    if (status === google.maps.places.PlacesServiceStatus.OK) {
        for (var i = 0; i < results.length; i++) {
            createMarker(results[i]);
        }
    }
}

function createMarker(place) {
    var placeLoc = place.geometry.location;
    var marker = new google.maps.Marker({
        map: map,
        icon: {
            url: "http://maps.google.com/mapfiles/ms/icons/blue-dot.png"
        },
        position: place.geometry.location
    });

    google.maps.event.addListener(marker, 'click', function () {
        infowindow.setContent(place.name);
        infowindow.open(map, this);
    });
}
function AutocompleteDirectionsHandler(map) {
    this.map = map;
    this.destinationPlaceId = null;
    this.travelMode = 'WALKING';
    var destinationInput = document.getElementById('destination-input');
    var modeSelector = document.getElementById('mode-selector');
    this.directionsService = new google.maps.DirectionsService;
    this.directionsDisplay = new google.maps.DirectionsRenderer;
    this.directionsDisplay.setMap(map);

    var destinationAutocomplete = new google.maps.places.Autocomplete(
        destinationInput, {placeIdOnly: true});

    this.setupClickListener('changemode-walking', 'WALKING');
    this.setupClickListener('changemode-transit', 'TRANSIT');
    this.setupClickListener('changemode-driving', 'DRIVING');

    this.setupPlaceChangedListener(destinationAutocomplete, 'DEST');

    this.map.controls[google.maps.ControlPosition.TOP_LEFT].push(destinationInput);
    this.map.controls[google.maps.ControlPosition.TOP_LEFT].push(modeSelector);
}

AutocompleteDirectionsHandler.prototype.setupClickListener = function(id, mode) {
    var radioButton = document.getElementById(id);
    var me = this;
    radioButton.addEventListener('click', function() {
        me.travelMode = mode;
        me.route();
    });
};

AutocompleteDirectionsHandler.prototype.setupPlaceChangedListener = function(autocomplete, mode) {
    var me = this;
    autocomplete.bindTo('bounds', this.map);
    autocomplete.addListener('place_changed', function() {
        var place = autocomplete.getPlace();
        if (!place.place_id) {
            window.alert("Please select an option from the dropdown list.");
            return;
        }
        if (mode === 'ORIG') {
            me.originPlaceId = place.place_id;
        } else {
            me.destinationPlaceId = place.place_id;
        }
        me.route();
    });

};

AutocompleteDirectionsHandler.prototype.route = function() {
    if (!this.destinationPlaceId) {
        return;
    }
    var me = this;

    this.directionsService.route({
        origin: myLatLng,
        destination: {'placeId': this.destinationPlaceId},
        travelMode: this.travelMode
    }, function(response, status) {
        if (status === 'OK') {
            me.directionsDisplay.setDirections(response);
        } else {
            window.alert('Directions request failed due to ' + status);
        }
    });
};


