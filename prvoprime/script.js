var worker;
var rangeMin;
var rangeMax;
var inputMin;
var inputMax;

var bCalculate;


window.onload = function() {
	worker = new Worker("prvocisla.js");
	rangeMin = document.getElementById("range-min")
	rangeMax = document.getElementById("range-max")
	inputMin = document.getElementById("input-min")
	inputMax = document.getElementById("input-max")
	bCalculate = document.getElementById("calculate")

	worker.onmessage = function(event) {		
    	
    	let percentage = document.getElementById("status");
    	if(event.data['percentage'] == 100)
    	{
    		percentage.innerHTML = "Vypočitané";
    		bCalculate.disabled = false;
    	}
    	else
    	{
	    	percentage.innerHTML = event.data['percentage'] + "%";
	    }

    	if(event.data['cislo'] != 0)
    	{
	    	let rdiv = document.getElementById("result")
	    	let res = document.createElement("li");
	    	res.innerHTML = event.data['cislo'];
	    	rdiv.appendChild(res);
	    }
	};

	//---------------------------------------------
	//---------------------------------------------
	rangeMin.oninput = function(data) {
		inputMin.valueAsNumber = rangeMin.valueAsNumber;
		rangeMax.min = rangeMin.valueAsNumber+1;
	}
	rangeMin.onchange = function(data)
	{
		inputMin.valueAsNumber = rangeMin.valueAsNumber;
		rangeMax.min = rangeMin.valueAsNumber+1;
	}
	inputMin.oninput = function(data) {
		if((inputMin.valueAsNumber)+1 >= rangeMax.valueAsNumber)
		{
			inputMin.valueAsNumber = rangeMax.valueAsNumber-1;
		}
		rangeMin.valueAsNumber = inputMin.valueAsNumber;
		rangeMax.min = rangeMin.valueAsNumber+1;
	}
	inputMin.onchange = function(data)
	{		
		if((inputMin.valueAsNumber)+1 >= rangeMax.valueAsNumber)
		{
			inputMin.valueAsNumber = rangeMax.valueAsNumber-1;
		}
		rangeMin.valueAsNumber = inputMin.valueAsNumber;
		rangeMax.min = rangeMin.valueAsNumber+1;
	}
	//---------------------------------------------
	//---------------------------------------------
	rangeMax.oninput = function(data) {
		inputMax.valueAsNumber = rangeMax.valueAsNumber;
		rangeMin.max = rangeMax.valueAsNumber-1;
	}
	rangeMax.onchange = function(data)
	{
		inputMax.valueAsNumber = rangeMax.valueAsNumber;
		rangeMin.max = rangeMax.valueAsNumber-1;
	}

	inputMax.oninput = function(data) {
		
		if(rangeMax.max < inputMax.value)
		{
			rangeMax.max = inputMax.value;
		}		
		rangeMax.valueAsNumber = inputMax.valueAsNumber;
		rangeMin.max = rangeMax.valueAsNumber-1;		
	}
	
	inputMax.onchange = function(data)
	{
		if((inputMax.valueAsNumber) <= rangeMin.valueAsNumber)
		{
			inputMax.valueAsNumber = rangeMin.valueAsNumber+1;
		}

		if(rangeMax.max < inputMax.value)
		{
			rangeMax.max = inputMax.value;
		}		
		rangeMax.valueAsNumber = inputMax.valueAsNumber;
		rangeMin.max = rangeMax.valueAsNumber-1;
	}
	//---------------------------------------------
	//---------------------------------------------
}

function startCalculation() {
	document.getElementById("result").innerHTML = "";
	bCalculate.disabled = true;
    let msg = {
    	'action': 'start',
    	'min': rangeMin.valueAsNumber,
    	'max': rangeMax.valueAsNumber
    }
	worker.postMessage(msg);
}