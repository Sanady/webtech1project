function jePrvocislo(cislo){
    var i = 2;
    while(i < cislo){
        if(cislo % i == 0){
            return false;
        }
        i++;
    }
    return true;
}

function najdiPrvocislo(lcislo, hcislo){
    if(lcislo > hcislo && lcislo == hcislo) return false;
    while(lcislo < hcislo){
        let cislo = 0;
        if(jePrvocislo(lcislo))
        {
            cislo = lcislo
        }
        
        var msg = {
            "percentage": (Math.round((lcislo/hcislo)*100)),
            "cislo": cislo
        };
        postMessage(msg);
        cislo = 0;
        lcislo++;        
    }
}

onmessage = function(event) {

    if(event.data['action'] == 'start')
    {
        console.log(event.data['action']);
        console.log('Správa z hlavného kodu.');
        let min = event.data['min'];
        let max = event.data['max'];
        najdiPrvocislo(min, max);
    }
}

