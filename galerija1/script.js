
var p = `{
    "photos": [
        {
          "src": "img/fotka1.jpg",
          "title": "Mesto",
          "description": "Pohľad z výšky"
        },
        {
          "src": "img/fotka2.jpg",
          "title": "Vodopád",
          "description": "Pohľad na vodopád."
        },
        {
          "src": "img/fotka3.jpg",
          "title": "Nevada",
          "description": "Death Valley"
				},
				{
          "src": "img/fotka4.jpg",
          "title": "Sloboda",
          "description": "Pohľad na vtáky"
				},
				{
          "src": "img/fotka5.jpg",
          "title": "Cesta",
          "description": "Lesná cesta"
        }      
      ]
  }`

var fotky = JSON.parse(p);
var slideIndex = 1;

var timer = false;


window.onload = function(e){ 
	srcPhoto = loadImage();
    loadModalImage();
    showSlides(slideIndex);
}

function loadImage(){
    for(i = 0; i < fotky.photos.length; i++)
    {
		console.log(fotky.photos[i].src);
		console.log(fotky.photos[i].title);
		console.log(fotky.photos[i].description);

        var image = document.createElement("img");        
				image.setAttribute("src", fotky.photos[i].src);
				image.setAttribute("onclick", "modalOpen(); slideIndex = "+ (i+1) +"; showSlides(slideIndex);");

        var gallery = document.getElementById("gallery");
				var li = document.createElement("li");

				gallery.appendChild(li);
				li.appendChild(image);
    }    
}

function loadModalImage(){
    for(i = 0; i < fotky.photos.length; i++)
    {
        var modal = document.getElementById("modal-vnutro");
        
        var slide = document.createElement("div");
        slide.setAttribute("class", "slide");
        slide.setAttribute("style", "display: none;");

        var title = document.createElement("p");
        title.setAttribute("class", "texttitle");
        title.innerHTML = fotky.photos[i].title;

        var image = document.createElement("img");        
        image.setAttribute("src", fotky.photos[i].src);
        image.setAttribute("class", "modal-fotka");

        var description = document.createElement("p");  
        description.setAttribute("class", "textdesc");
        description.innerHTML = fotky.photos[i].description;

        var left = document.createElement("a");
        left.setAttribute("class", "pred");
        left.setAttribute("onclick", "plusSlides(-1)");
        left.innerHTML = "&#10094;";

        var right = document.createElement("a");
        right.setAttribute("class","nasl");
        right.setAttribute("onclick", "plusSlides(1)");
        right.innerHTML = "&#10095;";
    

        modal.appendChild(slide);
        slide.appendChild(title);
        slide.appendChild(image);  
        slide.appendChild(left);
        slide.appendChild(right);
        slide.appendChild(description);
    }    
}

function modalOpen(){
  document.getElementById('mojModal').style.display = "block";
}

function modalClose(){
    document.getElementById('mojModal').style.display = "none";
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("slide");
  
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}

  console.log('Prikaz slide ' + slideIndex);


  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }
  slides[slideIndex-1].style.display = "block";
}

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function startSlideshow() {
	console.log("start slide show...");
	timer = window.setInterval(function() { plusSlides(1); }, 3000);	

	var control = document.getElementById("controll");
	controll.setAttribute("class", "stop");
	controll.setAttribute("onclick", "stopSlideshow();");
}

function stopSlideshow()
{
	clearInterval(timer);
	var control = document.getElementById("controll");
	controll.setAttribute("class", "play");
	controll.setAttribute("onclick", "startSlideshow();");
}