$(function(){
    
    var $cat = $("#city"),
        $subcat = $(".subcat");
    
    $cat.on("change",function(){
        var _rel = $(this).val();
        $subcat.find("option").attr("style","");
        $subcat.val("");
        if(!_rel) return $subcat.prop("disabled",true);
        $subcat.find("[rel="+_rel+"]").show();
        $subcat.prop("disabled",false);
    });
    
});

function zobrazPisaciBox() {
    var checkBox = document.getElementById("pivo2");
    var textBox = document.getElementById("textBox");
    if (checkBox.checked == true){
        textBox.style.display = "block";
    } else {
       textBox.style.display = "none";
    }
}

function zobrazPohlavieBox() {
    var radioButton = document.getElementById("pohlavie2");
    var doplBox = document.getElementById("doplBox");
    if (radioButton.checked == true){
        doplBox.style.display = "block";
    } else {
        doplBox.style.display = "none";
    }
}
