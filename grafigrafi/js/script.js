var listen = new EventSource('http://vmzakova.fei.stuba.sk/sse/sse.php');

listen.onopen = function(){
    console.log('DEBUG: Connectioned');
}

var config = {
    type: 'line',
    data: {
        labels: [],
        datasets: [{
            label: 'Data 1',
            fill: false,
            borderColor: window.chartColors.red,
            backgroundColor: window.chartColors.red,
            data: [],
            hidden: false
        }, {
            label: 'Data 2',
            fill: false,
            backgroundColor: window.chartColors.blue,
            borderColor: window.chartColors.blue,
            data: [],
            hidden: false
        }]
    },
    options: {
        responsive: true,
        title: {
            display: true,
            text: 'Graf:'
        },
        tooltips: {
            mode: 'index',
            intersect: false,
        },
        hover: {
            mode: 'nearest',
            intersect: true
        },
        scales: {
            xAxes: [{
                display: true,
                ticks: {
                    callback: function(dataLabel, index) {
                        return index % 2 === 0 ? dataLabel : '';
                    }
                },
                scaleLabel: {
                    display: true,
                    labelString: 'Horizontalna hodnota'
                }
            }],
            yAxes: [{
                display: true,
                beginAtZero: false,
                scaleLabel: {
                    display: true,
                    labelString: 'Vertikalna hodnota'
                }
            }]
        },
		pan: {
            enabled: true,
            mode: 'xy',
            rangeMin: {
                x: null,
                y: null
            },
            rangeMax: {
                x: null,
                y: null
            },
        },
        zoom: {
            enabled: true,
            mode: 'xy',
            rangeMin: {
                x: null,
                y: null
            },
            rangeMax: {
                x: null,
                y: null
            },
        }
    }
};

listen.onmessage = function(e){   
    var info = JSON.parse(e.data);

    console.log(info.x);
    console.log(info.y1);
    console.log(info.y2);

    if (config.data.datasets.length > 0) {

        config.data.labels.push(info.x);
        config.data.datasets[0].data.push(info.y1);
        config.data.datasets[1].data.push(info.y2);

        window.myLine.update();
    }
}

window.onload = function(){
    var ctx = document.getElementById("mainGraf").getContext('2d');
    window.myLine = new Chart(ctx, config);
    
    document.getElementById('stopGraf').addEventListener('click', function() {
        listen.close();
        console.log('DEBUG: Connection has been stopped!');
    });

    document.getElementById("cervena").addEventListener('click', function(){
        if(document.getElementById("cervena").checked == true){
            config.data.datasets[0].hidden = false;
            window.myLine.update();
            console.log('DEBUG: Red line is showed');
        }else{
            config.data.datasets[0].hidden = true;
            window.myLine.update();
            console.log('DEBUG: Red line is hidden');
        }
    
    });

    document.getElementById("modra").addEventListener('click', function(){
        if(document.getElementById("modra").checked == true){
            config.data.datasets[1].hidden = false;
            window.myLine.update();
            console.log('DEBUG: Blue line is showed');
        }else{
            config.data.datasets[1].hidden = true;
            window.myLine.update();
            console.log('DEBUG: Blue line is hidden');
        }
    });
}

