function reverseString(str) {
    var newString = "";
    for (var i = str.length - 1; i >= 0; i--) {
        newString += str[i];
    }
    return newString;
}

function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }

function myFunction(){
    var form = document.getElementById("form1").value;
    var place = document.getElementById("vnutro");
    var textP = document.getElementById("textReverse");

    var randomTop = Math.floor(Math.random() * 500) + 20;
    var randomLeft = Math.floor(Math.random() * 500) + 20;

    textP.style.top = randomTop+"px";
    console.log("Top: "+randomTop+"px");
    textP.style.left = randomLeft+"px";
    console.log("Left: "+randomLeft+"px");

    textP.onclick = function(){
        textP.style.backgroundColor = getRandomColor();
    }

    textP.innerHTML = reverseString(form); 
    
    place.appendChild(textP);
}